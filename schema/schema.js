const {gql} = require('apollo-server-express')

const typeDefs = gql`
    type Post {
        id: ID
        title: String
        photo: String
        ownerID: User
    }

    type File {
        filename: String!
        mimetype: String!
        encoding: String!
    }

    type User {
        id: ID!
        name: String
        photo: String
        posts: [Post]
    }

    # ROOT TYPE 
    type Query {
        _: Boolean
        uploads: [File]
        posts: [Post]
        post (id: ID!): Post
        users: [User]
        user (id: ID!): User
    }

    type Mutation {
        uploadPhoto(photo: String): String
        singleUpload(file: Upload!): File!
        createUser(name: String , photo: String) : User
        createPost(title: String , ownerID: ID!) : Post
    }
`

module.exports = typeDefs