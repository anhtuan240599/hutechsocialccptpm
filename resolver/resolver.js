const { mongo } = require("mongoose");
require("dotenv").config();
const Post = require("../models/Post");
const User = require("../models/User");
const cloudinary = require("cloudinary");
const resolvers = {
  //Query
  Query: {
    uploads: (parent, args) => {},
    posts: async (parent, args, { mongoDataMethods }) =>
      await mongoDataMethods.getAllPosts(),
    post: async (parent, { id }, { mongoDataMethods }) =>
      await mongoDataMethods.getPostByID(id),
    users: async (parent, args, { mongoDataMethods }) =>
      await mongoDataMethods.getAllUsers(),
    user: async (parent, { id }, { mongoDataMethods }) =>
      await mongoDataMethods.getUserByID(id),
  },
  Post: {
    ownerID: async ({ ownerID }, args, { mongoDataMethods }) =>
      await mongoDataMethods.getUserByID(ownerID),
  },
  User: {
    posts: async ({ id }, args, { mongoDataMethods }) =>
      await mongoDataMethods.getAllPosts({ ownerID: id }),
  },

  // Mutation
  Mutation: {
    uploadPhoto: async (_, { photo }) => {
      //initialize cloudinary
      cloudinary.config({
        cloud_name: process.env.CLOUDINARY_NAME,
        api_key: process.env.CLOUDINARY_API_KEY,
        api_secret: process.env.CLOUDINARY_API_SECRET,
      });
      /*
            try-catch block for handling actual image upload
            */
      try {
        var result = await cloudinary.v2.uploader.upload(photo, {
          //here i chose to allow only jpg and png upload
          allowed_formats: ["jpg", "png"],
          //generates a new id for each uploaded image
          public_id: "",
          /*creates a folder called "your_folder_name" where images will be stored.
           */
          folder: "your_folder_name",
        });
      } catch (e) {
        //returns an error message on image upload failure.
        return `Image could not be uploaded:${e.message}`;
      }
      /*returns uploaded photo url if successful `result.url`.
            if we were going to store image name in database,this
            */
      return `Successful-Photo URL: ${result.url}`;
    },
    createUser: async (parent, args, { mongoDataMethods }) => {
      cloudinary.config({
        cloud_name: process.env.CLOUDINARY_NAME,
        api_key: process.env.CLOUDINARY_API_KEY,
        api_secret: process.env.CLOUDINARY_API_SECRET,
      });
      try {
        var result = await cloudinary.v2.uploader.upload(args.photo, {
          allowed_formats: ["jpg", "png"],
          public_id: "",
          folder: "your_folder_name",
        });
      } catch (e) {
        return `Image could not be uploaded:${e.message}`;
      }
    args.photo = result.url
      await mongoDataMethods.createUser(args);
    },

    createPost: async (parent, args, { mongoDataMethods }) =>
      await mongoDataMethods.createPost(args),
  },
};

module.exports = resolvers;
