const mongoose = require('mongoose');
const Schema = mongoose.Schema

const PostSchema = new Schema({
    title: {
        type: String
    },
    ownerID: {
        type: String
    },
    image: {
        type : String
    }


}) 

module.exports = mongoose.model('Post' , PostSchema)