const posts = [
    {
        id: 1,
        title: 'Test',
        ownerID: 1
    },
    {
        id: 2,
        title: 'Test database using graphql',
        ownerID: 2
    },
    {
        id: 3,
        title: 'Test 2',
        ownerID: 1
    },
]

const users = [
    {
        id: 1,
        name: 'Huynh Anh Tuan'
    },
    {
        id: 2,
        name: 'Le Cao Van Son'
    }
]

module.exports = {
    posts,
    users
}