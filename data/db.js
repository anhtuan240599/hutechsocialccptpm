const User  =  require('../models/User')
const Post  = require('../models/Post')

const  mongoDataMethods =  {
    getAllPosts: async (condition = null) => condition == null ? await Post.find() : await Post.find(condition) ,
    getPostByID: async id => await Post.findById(id),
    getAllUsers: async () => await User.find(),
    getUserByID: async id => await User.findById(id),
    createUser: async args => {
        const newUser = new User(args) 
        return await newUser.save()
    },
    createPost: async args => {
        const newPost = new Post(args) 
        return await newPost.save()
    }
}

module.exports = mongoDataMethods