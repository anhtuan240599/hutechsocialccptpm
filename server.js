const express = require('express');
require("dotenv").config();
const {ApolloServer} = require('apollo-server-express');
const mongoose = require('mongoose');

//Load schema & resolvers
const typeDefs = require('./schema/schema')
const resolvers = require('./resolver/resolver')

//Create server Apollo
const server = new ApolloServer({
    typeDefs,
    resolvers,
    playground: true,
    context: () => ({mongoDataMethods})
})

//Connect to MongoDB
mongoose.connect('mongodb+srv://admin:admin@cluster0.5deoh.mongodb.net/alo?retryWrites=true&w=majority',{
    useCreateIndex: true,
    useFindAndModify: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
    .then(() => console.log('Connected to database'))
    .catch((err) => console.log(err))

// Load db methods
const mongoDataMethods = require('./data/db')

const app = express()
server.applyMiddleware({app})

app.listen({port:4000}, ()=>{
    console.log(`Server listening at http://localhost:4000${server.graphqlPath}`) 
})